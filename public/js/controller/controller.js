'use strict';

var PersonCtrl = function($scope, PersonFactory){
    // Code goes here.
    init();

    $scope.Clear = function(){
    	$scope.person = null;
    }

    $scope.save = function(){
    	
        if($scope.person._id == null){
        	PersonFactory.savePerson($scope.person,function(data){
	            init();
	        });
        }else{
        	PersonFactory.updatePerson($scope.person._id, $scope.person, function(data){
        		init();
        	});
        }
    }

    $scope.deletebrand = function(id){
    	PersonFactory.deletePerson(id,function(data){
            init();
        });
    }

    $scope.edit = function(id){
    	console.log(id);
    	PersonFactory.getSepcific(id, function(data){
    		$scope.person = data.result;
    	});
    }

    function init(){
    	$scope.person = {};
    	PersonFactory.getPerson(function(data){
            $scope.persons = data.result;
        });
    }
}

scaffold.Controllers.controller('PersonCtrl', ['$scope', 'PersonFactory',PersonCtrl]);
