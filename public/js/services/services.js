'use strict';

var PersonFactory = function($http){
	var PersonFactory = {};

	PersonFactory.getPerson = function(cb){
		$http.get('/Person').success(function(data){
			cb(data);
		}).error(function(err){
			cb(err);
		});
	};

	PersonFactory.getSepcific = function(id, cb){
		$http.get('/Person/'+ id).success(function(data){
			cb(data);
		}).error(function(err){
			cb(err);
		});
	};

	PersonFactory.savePerson = function(data, cb){
		$http.post('/Person', data).success(function(data){
			cb(data);
		}).error(function(err){
			cb(err);
		});
	};

	PersonFactory.updatePerson = function(id, data, cb){
		$http.put('/Person/'+ id, data).success(function(data){
			cb(data);
		}).error(function(err){
			cb(err);
		});
	};

	PersonFactory.deletePerson = function(id, cb){
		$http.delete('/Person/'+ id).success(function(data){
			cb(data);
		}).error(function(err){
			cb(err);
		});
	};

	return PersonFactory;
}

scaffold.Services.factory('PersonFactory',['$http' ,PersonFactory]);

