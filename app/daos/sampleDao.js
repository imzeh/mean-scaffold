'use strict';

var mongoose = require('mongoose');
var Person = mongoose.model('Person');

exports.getPerson = function getPerson(next){
	Person.find(next);
};

exports.getSpecific = function getSpecific(id, next){
	Person.findById(id, next);
};

exports.savePerson = function savePerson(data, next){
	Person.create(data, next);
}

exports.updatePerson = function updatePerson(id, data, next){
	Person.findByIdAndUpdate(id, data, next);
}

exports.deletePerson = function deletePerson(id, next){
	Person.findOneAndRemove(id, next);
}

