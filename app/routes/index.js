'use strict';

var ctrl = require('../controllers/');

module.exports = function(app, passport, isLoggedIn, isAdmin) {
    // Code goes Here.
    app.route('/').get(function(req, res) {
        res.render('index');
    });

    app.route('/Person')
    	.get(ctrl.getPerson)
    	.post(ctrl.savePerson);

    app.route('/Person/:id')
        .get(ctrl.getSpecific)
    	.put(ctrl.updatePerson)
    	.delete(ctrl.deletePerson);
    	
};
